// ==UserScript==
// @name         Remove ComicRocket banner
// @namespace    http://web.programdotrun.com
// @version      1.1
// @description  Remove the Comic-Rocket banner from read pages
// @author       Mackenzie Zastrow
// @match        http://www.comic-rocket.com/read/*
// @downloadURL  https://bitbucket.org/zastrowm/tampermonkey/raw/master/comic-rocket-hide-banner.user.js
// @grant        none
// ==/UserScript==

function update() {
  var it = document.querySelector("#serialpagebody");
  it.style.top = 0;
}

update();
setTimeout(update, 100);
setTimeout(update, 5 * 60 * 1000);
