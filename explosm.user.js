// ==UserScript==
// @name         Explosm Smaller Comics
// @description  Trim down unrelated items
// @match        http://explosm.net/comics/*
// @downloadURL  https://bitbucket.org/zastrowm/tampermonkey/raw/master/explosm.user.js
// @grant        none
// @version      1.0
// @namespace    http://web.programdotrun.com
// @author       Mackenzie Zastrow
// ==/UserScript==

(function() {
    'use strict';
    
    document.querySelector("#superheader-bar").style.display = "none";
    document.querySelector(".header-leaderboard-container").style.display = "none";
    document.querySelector(".nav-buttons-top").style.display = "none";
    // Your code here...
})();