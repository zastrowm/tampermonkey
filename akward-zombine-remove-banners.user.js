// ==UserScript==
// @name         Awkard Zombie Remove Banners
// @namespace    http://web.programdotrun.com
// @version      1.1
// @description  Remove the Awkard Zombie Remove Banners
// @author       Mackenzie Zastrow
// @match        http://www.awkwardzombie.com/index.php?page=*&comic=*
// @downloadURL  https://bitbucket.org/zastrowm/tampermonkey/raw/master/akward-zombine-remove-banners.user.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    document.querySelector("#banner").remove();
    document.querySelector("#imgctrls").remove();
    document.querySelector("#content").style.paddingTop = "0px";
    
})();