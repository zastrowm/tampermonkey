// ==UserScript==
// @name         Feedly Background Tab
// @namespace    http://web.programdotrun.com
// @version      1.8
// @description  Open Feedly Items in a Background tab
// @author       Mackenzie Zastrow
// @downloadURL  https://bitbucket.org/zastrowm/tampermonkey/raw/master/feedly.user.js
// @match        *://*.feedly.com/*
// @match        *://feedly.com/*
// @grant        GM_openInTab
// ==/UserScript==
/* jshint -W097 */
'use strict';

function openTab(url) {
     GM_openInTab(url, {
        active: false,
        insert: false
    });
}

function openOne() {
    var url = document.querySelector(".selected .content a").href;
    openTab(url);
}

function openAll() {
    var all = document.querySelectorAll(".entry");
    
    for (var i = 0; i < all.length; i++) {
        let element = all[i];
        setTimeout(function() {
           openTab(element.dataset.alternateLink);

        }, i * 9*1000);
    }
}

var isCtrl = false;

document.addEventListener("keydown", function(args) { 

    if (args.keyCode == 17) {
    	isCtrl = true;
    } 
})

document.addEventListener("keyup", 
function(args) {
 
  if (args.key !== ":" && args.key !== ";")
    return;
    
  if (args.ctrlKey) {
    openAll();
  } else { 
    openOne();
  }
})