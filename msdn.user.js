// ==UserScript==
// @name         MSDN Documentation Enhancements
// @namespace    http://web.programdotrun.com
// @version      1.2
// @description  MSDN Documentation Enhancements
// @author       Mackenzie Zastrow
// @match        https://msdn.microsoft.com/en-us/library/*
// @match        http://msdn.microsoft.com/en-us/library/*
// @downloadURL  https://bitbucket.org/zastrowm/tampermonkey/raw/master/msdn.user.js
// @grant        none
// ==/UserScript==

function highlightUnique(selector) {
  var all = document.querySelectorAll(selector)
  for (var i = 0; i < all.length; i++) {
    var item = all[i];
    if (item.children[2].textContent.indexOf("(Inherited from") == -1) {
      item.style.backgroundColor = "#eee";
    }
  }
}

function update() {
  highlightUnique("#idProperties tr");
  highlightUnique("#idMethods tr");
  highlightUnique("#idFields tr");
  highlightUnique("#idEvents tr");
}

update();
setTimeout(update, 100);
setTimeout(update, 5 * 60 * 1000);