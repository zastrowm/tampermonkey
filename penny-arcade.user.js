// ==UserScript==
// @name         Remove Penny Arcade Fluff
// @namespace    http://web.programdotrun.com
// @version      1.6
// @description  Remove Penny Arcade Fluff
// @author       Mackenzie Zastrow
// @match        *://penny-arcade.com/comic/*
// @match        *://*.penny-arcade.com/comic/*
// @downloadURL  https://bitbucket.org/zastrowm/tampermonkey/raw/master/penny-arcade.user.js
// @grant        none
// ==/UserScript==

function setProperty(query, propname, value) {
    var elements = document.querySelectorAll(query);   
    
    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];

            element.style[propname] = value;
            console.log("setting", propname, element);
    }
}

function update() {
  var ids = ["bb-sticky-wrapper", "navTop", "header", "header-sticky-wrapper"];
    
  for (var i = 0; i < ids.length; i++) {
    var id = ids[i];
      var element = document.querySelector("#" + id);
      console.log(element);
      if (element != null)
      {
          element.style.display = "none";
      }
  }
    
    setProperty("#main", "padding", "0");
    setProperty(".navTop", "display", "none");
    setProperty("input.btn.btnBuyPrint", "display", "none");
    setProperty(".copy.comicTag > h2", "width", "1000px");
}

update();
setTimeout(update, 100);
setTimeout(update, 5 * 60 * 1000);
