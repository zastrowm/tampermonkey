// ==UserScript==
// @name         Pinterest Pin Website Redirect
// @namespace    http://web.programdotrun.com
// @version      1.3
// @description  Redirect https://www.pinterest.com/pin urls to the actual websites
//               that the pin belongs to
// @author       Mackenzie Zastrow
// @match        https://www.pinterest.com/pin/*/
// @downloadURL  https://bitbucket.org/zastrowm/tampermonkey/raw/master/pinterest-pin-redirect.user.js
// @grant        none
// ==/UserScript==

setTimeout(function() {
  var element = document.querySelector("a.Button.website.pinActionBarButton[rel=nofollow]");
  if (element != null) {
    window.location.href = element.href;
  }
}, 100);
